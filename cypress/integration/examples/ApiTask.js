describe("API Testing", () => {
    it("Should list users", () => {
      cy.request("GET", "https://reqres.in/api/users").then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body.data).to.be.an("array").and.not.empty;
      });
    });
  
    it("Should retrieve a single user", () => {
      cy.request("GET", "https://reqres.in/api/users/1").then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body.data.id).to.eq(1);
      });
    });
  
    it("Should return 404 for non-existing user", () => {
      cy.request({ method: "GET", url: "https://reqres.in/api/users/1000", failOnStatusCode: false }).then((response) => {
        expect(response.status).to.eq(404);
      });
    });
  
    it("Should create a new user", () => {
      const newUser = {
        name: "John Doe",
        job: "Tester",
      };
  
      cy.request("POST", "https://reqres.in/api/users", newUser).then((response) => {
        expect(response.status).to.eq(201);
        expect(response.body.name).to.eq(newUser.name);
        expect(response.body.job).to.eq(newUser.job);
      });
    });
  
    it("Should update an existing user", () => {
      const updatedUser = {
        name: "Updated Name",
        job: "Updated Job",
      };
  
      cy.request("PUT", "https://reqres.in/api/users/2", updatedUser).then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body.name).to.eq(updatedUser.name);
        expect(response.body.job).to.eq(updatedUser.job);
      });
    });
  
    it("Should return 400 for invalid user update", () => {
      const invalidUser = {
        job: "Updated Job",
      };
  
      cy.request({ method: "PUT", url: "https://reqres.in/api/users/2", body: invalidUser, failOnStatusCode: false }).then((response) => {
        expect(response.status).to.eq(400);
      });
    });
  
    it("Should delete a user", () => {
      cy.request("DELETE", "https://reqres.in/api/users/3").then((response) => {
        expect(response.status).to.eq(204);
      });
    });
  
    it("Should return 404 for deleting a non-existing user", () => {
      cy.request({ method: "DELETE", url: "https://reqres.in/api/users/1000", failOnStatusCode: false }).then((response) => {
        expect(response.status).to.eq(404);
      });
    });
  
    it("Should list users on page 2", () => {
      cy.request("GET", "https://reqres.in/api/users?page=2").then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body.page).to.eq(2);
        expect(response.body.data).to.be.an("array").and.not.empty;
      });
    });
  
    it("Should return 200 for delayed response", () => {
      cy.request("GET", "https://reqres.in/api/users?delay=3").then((response) => {
        expect(response.status).to.eq(200);
      });
    });
  
    it("Should retrieve user's details using a resource URL", () => {
      cy.request("GET", "https://reqres.in/api/unknown/2").then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body.data.id).to.eq(2);
      });
    });
  
    it("Should return 400 for an invalid resource URL", () => {
      cy.request({ method: "GET", url: "https://reqres.in/api/unknown/invalid", failOnStatusCode: false }).then((response) => {
        expect(response.status).to.eq(400);
      });
    });
  
    it("Should list all resources", () => {
      cy.request("GET", "https://reqres.in/api/unknown").then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body.data).to.be.an("array").and.not.empty;
      });
    });
  });
  